import os
import pathlib
import datetime

from dotenv import dotenv_values
from loguru import logger

now: datetime.datetime = datetime.datetime.now()
DEFAULT_LOG_PATH: str = '/opt/geoip/logs/geoip_%s_%s_%s-%s-%s.log' % (now.year, now.month, now.day, now.hour, now.minute)

BASE_DIR = pathlib.Path(__file__).parent.parent
envpath = BASE_DIR.joinpath('.env')
print(envpath.absolute())
config = {
    **dotenv_values(envpath),  # load shared development variables
    **os.environ,  # override loaded values with environment variables
}

logger.add(
    config.get('LOG_PATH') or DEFAULT_LOG_PATH,
    level='INFO',
    rotation='1 week',
    backtrace=True,
    diagnose=True)
